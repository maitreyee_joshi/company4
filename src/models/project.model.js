var dbConn  = require('../../config/db.config');

var ProjectList = function(project){

    this.project_name     =   project.project_name;
    this.project_status   =   project.project_status;
    this.project_manager  =   project.project_manager;
    this.project_leader   =   project.project_leader;
    this.project_employee =   project.project_employee;
}

// get all employees
ProjectList.getAllProjects = (result) =>{
    dbConn.query('SELECT * FROM project_table', (err, res)=>{
        if(err){
            console.log('Error while fetching projects', err);
            result(null,err);
        }else{
            console.log('projects fetched successfully');
            result(null,res);
        }
    })
}

// get employee by ID from DB
ProjectList.getProjectByID = (id, result)=>{
    dbConn.query('SELECT * FROM project_table WHERE pid=?', id, (err, res)=>{
        if(err){
            console.log('Error while fetching projects by id', err);
            result(null, err);
        }else{
            result(null, res);
        }
    })
}

// create new employee
ProjectList.createProjects = (employeeReqData, result) =>{
    dbConn.query('INSERT INTO project_table SET ? ', employeeReqData, (err, res)=>{
        if(err){
            console.log('Error while inserting data');
            result(null, err);
        }else{
            console.log('new list created successfully');
            result(null, res)
        }
    })
}

// update employee
ProjectList.updateProjects = (id, employeeReqData, result)=>{
    dbConn.query("UPDATE project_table SET project_name=?,project_status=?,project_manager=?,project_teamleader=?,project_employee=? WHERE pid = ?", [employeeReqData.project_name,employeeReqData.project_status,employeeReqData.project_manager, employeeReqData.project_leader,employeeReqData.project_employee,pid], (err, res)=>{
        if(err){
            console.log('Error while updating the list');
            result(null, err);
        }else{
            console.log("List updated successfully");
            result(null, res);
        }
    });
}

// delete employee
ProjectList.deleteProjects = (id, result)=>{
    dbConn.query('DELETE FROM project_table WHERE pid=?', [id], (err, res)=>{
         if(err){
             console.log('Error while deleting the employee');
             result(null, err);
         }else{
             result(null, res);
         }
     })
}

module.exports = ProjectList;