var dbConn  = require('../../config/db.config');

var Client = function(employee){
    this.client_name         =   employee.client_name;
    this.client_type         =   employee.client_type;
    this.client_status       =   employee.client_status;
}

// get all employees
Client.getAllClient = (result) =>{
    dbConn.query('SELECT * FROM client_table', (err, res)=>{
        if(err){
            console.log('Error while fetching client', err);
            result(null,err);
        }else{
            console.log('client fetched successfully');
            result(null,res);
        }
    })
}

// get employee by ID from DB
Client.getClientByID = (id, result)=>{
    dbConn.query('SELECT * FROM client_table WHERE cid=?', id, (err, res)=>{
        if(err){
            console.log('Error while fetching client by id', err);
            result(null, err);
        }else{
            result(null, res);
        }
    })
}

// create new employee
Client.createClient = (employeeReqData, result) =>{
    dbConn.query('INSERT INTO client_table SET ? ', employeeReqData, (err, res)=>{
        if(err){
            console.log('Error while inserting data');
            result(null, err);
        }else{
            console.log('client created successfully');
            result(null, res)
        }
    })
}

// update employee
Client.updateClient = (id, employeeReqData, result)=>{
    dbConn.query("UPDATE employees SET client_name=?,client_type=?,client_status=? WHERE cid = ?", [employeeReqData.client_name,employeeReqData.client_type,employeeReqData.client_status, cid], (err, res)=>{
        if(err){
            console.log('Error while updating the employee');
            result(null, err);
        }else{
            console.log("Employee updated successfully");
            result(null, res);
        }
    });
}

// delete employee
Client.deleteClient = (id, result)=>{
    dbConn.query('DELETE FROM client_table WHERE cid=?', [id], (err, res)=>{
         if(err){
             console.log('Error while deleting the employee');
             result(null, err);
         }else{
             result(null, res);
         }
     })
}

module.exports = Client;