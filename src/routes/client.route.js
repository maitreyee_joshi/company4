const express = require('express');
const router = express.Router();

const clientController = require('../controllers/client.controller');

const{addClientValidation} = require('../../src/validation/client/client.validation');
// get all employees
router.get('/', clientController.getClientList);

// get employee by ID
router.get('/:id',clientController.getClientByID);

// create new employee
router.post('/',addClientValidation , clientController.createNewClient);

// update employee
router.put('/:id', addClientValidation , clientController.updateClient);

// delete employee
router.delete('/:id',clientController.deleteClient);

module.exports = router;