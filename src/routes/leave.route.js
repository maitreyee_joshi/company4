const express = require('express');
const router = express.Router();

const leaveController = require('../controllers/leave.controller');

const{addLeaveValidation} = require('../../src/validation/leave/leave.validation');

// get all employees
router.get('/', leaveController.getLeaveList);

// get employee by ID
router.get('/:id',leaveController.getLeaveByID);

// create new employee
router.post('/', addLeaveValidation , leaveController.createNewLeave );

// update employee
router.put('/:id',addLeaveValidation , leaveController.updateLeave);

// delete employee
router.delete('/:id',leaveController.deleteLeave);

module.exports = router;