//add_accessories
const express = require('express');
const router = express.Router();

const add_accessoriessController = require('../controllers/add_accessories.controller');

// get all add_accessories
router.get('/', add_accessoriessController.add_accessoriessList);

// get add_accessories by ID
router.get('/:id',add_accessoriessController.getadd_accessoriesByID);

// create new add_accessories
router.post('/', add_accessoriessController.createNewadd_accessories);

// update add_accessories
router.put('/:id', add_accessoriessController.updateadd_accessories);

// delete add_accessories
router.delete('/:id',add_accessoriessController.deleteadd_accessories);

module.exports = router;