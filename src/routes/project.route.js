const express = require('express');
const router = express.Router();

const projectsController = require('../controllers/project.controller');

const{addProjectValidation} = require('../../src/validation/project/project.validation');
// get all employees
router.get('/', projectsController.getProjectsList);

// get employee by ID
router.get('/:id',projectsController.getProjectsByID);

// create new employee
router.post('/',addProjectValidation , projectsController.createNewProjects);

// update employee
router.put('/:id',addProjectValidation , projectsController.updateProjects);

// delete employee
router.delete('/:id',projectsController.deleteProjects);

module.exports = router;