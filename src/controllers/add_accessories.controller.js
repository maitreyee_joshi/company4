const add_accessoriesModel = require('../models/add_accessories.model');
const Joi = require('joi');

// get all add_accessories list add_accessories
exports.add_accessoriessList = (req, res)=> {
    //console.log('here all add_accessories list');
    add_accessoriesModel.getAlladd_accessoriess((err, add_accessoriess) =>{
        console.log('We are here');
        if(err)
        res.send(err);
        console.log('add_accessories', add_accessoriess);
        res.send(add_accessoriess)
    })
}

// get add_accessories by ID
exports.getadd_accessoriesByID = (req, res)=>{
    //console.log('get emp by id');
    add_accessoriesModel.getadd_accessoriesByID(req.params.id, (err, add_accessories)=>{
        if(err)
        res.send(err);
        console.log('single employee data',add_accessories);
        res.send(add_accessories);
    })
}


// create new add_accessories
exports.createNewadd_accessories = (req, res) =>{

    const schema = Joi.object().keys({
        employee_id : Joi.number().required(),
        employee_name : Joi.string().max(100).required(),
        accessory_name : Joi.string().max(100).required(),
        accessory_quantity : Joi.number().required(),
        accessory_status : Joi.number().required()

    });
    Joi.validate(req.body,schema,(err,result)=>{
        if(err){
            console.log(err);
            res.send('an error has occured');
        }
        console.log(result);
        res.send('succesfully posted data');
    })

    const add_accessoriesReqData = new add_accessoriesModel(req.body);
    console.log('add_accessoriesReqData', add_accessoriesReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        add_accessoriesModel.createadd_accessories(add_accessoriesReqData, (err, add_accessories)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'add_accessories Created Successfully', data: add_accessories.insertId})
        })
    }
}


// update add_accessories
exports.updateadd_accessories = (req, res)=>{
    const add_accessoriesReqData = new add_accessoriesModel(req.body);
    console.log('add_accessoriesReqData update', add_accessoriesReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        add_accessoriesModel.updateadd_accessories(req.params.id, add_accessoriesReqData, (err, add_accessories)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'add_accessories updated Successfully'})
        })
    }
}


// delete add_accessories
exports.deleteadd_accessories = (req, res)=>{
    add_accessoriesModel.deleteadd_accessories(req.params.id, (err, add_accessories)=>{
        if(err)
        res.send(err);
        res.json({success:true, message: 'add_accessories deleted successully!'});
    })
}