const completed_taskModel = require('../models/completed_task.model');
const Joi = require('joi');

// get all completed_task list completed_task
exports.completed_tasksList = (req, res)=> {
    //console.log('here all completed_task list');
    completed_taskModel.getAllcompleted_tasks((err, completed_tasks) =>{
        console.log('We are here');
        if(err)
        res.send(err);
        console.log('completed_task', completed_tasks);
        res.send(completed_tasks)
    })
}

// get completed_task by ID
exports.getcompleted_taskByID = (req, res)=>{
    //console.log('get emp by id');
    completed_taskModel.getcompleted_taskByID(req.params.id, (err, completed_task)=>{
        if(err)
        res.send(err);
        console.log('single employee data',completed_task);
        res.send(completed_task);
    })
}


// create new completed_task
exports.createNewcompleted_task = (req, res) =>{

    const schema = Joi.object().keys({
        employee_id : Joi.number().required(),
        employee_name : Joi.string().max(100).required(),
        task : Joi.string().max(100).required(),
        completion_status : Joi.number().required()
    });
    Joi.validate(req.body,schema,(err,result)=>{
        if(err){
            console.log(err);
            res.send('an error has occured');
        }
        console.log(result);
        res.send('succesfully posted data');
    })

    const completed_taskReqData = new completed_taskModel(req.body);
    console.log('completed_taskReqData', completed_taskReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        completed_taskModel.createcompleted_task(completed_taskReqData, (err, completed_task)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'completed_task Created Successfully', data: completed_task.insertId})
        })
    }
}


// update completed_task
exports.updatecompleted_task = (req, res)=>{
    const completed_taskReqData = new completed_taskModel(req.body);
    console.log('completed_taskReqData update', completed_taskReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        completed_taskModel.updatecompleted_task(req.params.id, completed_taskReqData, (err, completed_task)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'completed_task updated Successfully'})
        })
    }
}


// delete completed_task
exports.deletecompleted_task = (req, res)=>{
    completed_taskModel.deletecompleted_task(req.params.id, (err, completed_task)=>{
        if(err)
        res.send(err);
        res.json({success:true, message: 'completed_task deleted successully!'});
    })
}