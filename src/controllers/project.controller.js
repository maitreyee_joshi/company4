
const ProjectsModel = require('../models/project.model');

// get all employee list
exports.getProjectsList = (req, res)=> {
    //console.log('here all employees list');
    ProjectsModel.getAllProjects((err, employees) =>{
        console.log('We are here');
        if(err)
        res.send(err);
        console.log('Projects', employees);
        res.send(employees)
    })
}

// get employee by ID
exports.getProjectsByID = (req, res)=>{
    //console.log('get emp by id');
    ProjectsModel.getProjectsByID(req.params.id, (err, employee)=>{
        if(err)
        res.send(err);
        console.log('single project data',employee);
        res.send(employee);
    })
}

// create new employee
exports.createNewProjects = (req, res) =>{
    const employeeReqData = new ProjectsModel(req.body);
    console.log('projectReqData', employeeReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        ProjectsModel.createProjects(employeeReqData, (err, employee)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'Project list added Successfully', data: employee.insertId})
        })
    }
}

// update employee
exports.updateProjects = (req, res)=>{
    const employeeReqData = new ProjectsModel(req.body);
    console.log('employeeReqData update', employeeReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        ProjectsModel.updateProjects(req.params.id, employeeReqData, (err, employee)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'Project list updated Successfully'})
        })
    }
}

// delete employee
exports.deleteProjects = (req, res)=>{
    ProjectsModel.deleteProjects(req.params.id, (err, employee)=>{
        if(err)
        res.send(err);
        res.json({success:true, message: 'Project deleted successully!'});
    })
}