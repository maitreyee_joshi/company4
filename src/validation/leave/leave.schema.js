const joi = require("@hapi/joi");

const schema = {
    leave : joi.object({
        leave_whosetaken : joi.number().integer().max(11).required(),
        leave_fromdate : joi.string().max(10).required(),
        leave_todate : joi.string().max(10).required(),
        leave_status : joi.string().max(100).required(),
        leave_whoseapprove : joi.number().integer().max(11).required(),
        leave_rejection_desc : joi.string().max(100).required()
    })
};

module.exports = schema ;