const {leave} = require("./leave.schema");

module.exports={
    addLeaveValidation : async(req, res, next) =>{
        const value = await leave.validate(req.body);
        if (value.error){
            res.json({
                success : 0,
                message : value.error.details[0].message
            })
        }else{
            next();
        }
    }
}