const joi = require("@hapi/joi");

const schema = {
    client : joi.object({
        cid : joi.number().integer().max(11).required(),
        client_name : joi.string().max(45).required(),
        client_type : joi.number().integer().max(11).required(),
        client_status : joi.number().integer().max(11).required()
    })
};

module.exports = schema ;