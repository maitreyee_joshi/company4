const {client} = require("./client.schema");

module.exports={
    addClientValidation : async(req, res, next) =>{
        const value = await client.validate(req.body);
        if (value.error){
            res.json({
                success : 0,
                message : value.error.details[0].message
            })
        }else{
            next();
        }
    }
}