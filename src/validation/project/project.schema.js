const joi = require("@hapi/joi");

const schema = {
    project : joi.object({
        pid : jovi.number().integer().max(11).required(),
        project_name : jovi.string().max(45).required(),
        project_status : jovi.number().integer().max(11).required(),
        project_manager : jovi.number().integer().max(11).required(),
        project_teamleader : jovi.number().integer().max(11).required(),
        project_employee : jovi.number().integer().max(11).required()
    })
};

module.exports = schema ;