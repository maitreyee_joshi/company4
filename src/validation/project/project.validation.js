const {project} = require("./project.schema");

module.exports={
    addProjectValidation : async(req, res, next) =>{
        const value = await project.validate(req.body);
        if (value.error){
            res.json({
                success : 0,
                message : value.error.details[0].message
            })
        }else{
            next();
        }
    }
}